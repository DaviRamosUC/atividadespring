package br.ucsal.atividade.atividade.services;

import br.ucsal.atividade.atividade.entities.Contato;
import br.ucsal.atividade.atividade.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ContatoService {

    @Autowired
    ContatoRepository contatoRepository;

    public List<Contato> findAll() {
        return contatoRepository.findAll();
    }

    public Contato findById(Long id){
        Optional<Contato> obj = contatoRepository.findById(id);
//        return obj.orElseThrow(() -> new ResourceNotFoundException(id));
        return obj.orElse(null);
    }

    public List<Contato> findByNome(String name){
        return contatoRepository.findByNome(name);
    }

    public Contato save(Contato contato){
        return contatoRepository.save(contato);
    }

    public String deleteById(Long id){
        try {
            contatoRepository.deleteById(id);
        }catch (Exception e){
            return "Não foi possível achar o contato com id:" + id;
        }
        return "Exclusão feita com sucesso";
    }

    public Contato update(Long id, Contato obj){
        try{
            Contato contato = contatoRepository.getById(id);
            updateData(contato, obj);
            return  contatoRepository.save(contato);
        }catch (Exception e){
            return null;
        }
    }

    private void updateData(Contato contato, Contato obj) {
        contato.setNome(obj.getNome());
        contato.setEmail(obj.getEmail());
        contato.setEndereco(obj.getEndereco());
        contato.setDataNascimento(obj.getDataNascimento());
    }
}
