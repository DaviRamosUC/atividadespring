package br.ucsal.atividade.atividade.controllers;

import br.ucsal.atividade.atividade.entities.Contato;
import br.ucsal.atividade.atividade.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ContatoController {

    @Autowired
    private ContatoService contatoService;

//    Métodos expositores de form's

    @GetMapping(value = "/procuraContatoForm")
    public String showFormContato() {
        return "procuraContato";
    }

    @GetMapping(value = "/procuraNome")
    public String showFormNome() {
        return "procuraNome";
    }

    @GetMapping(path = "/contatoForm")
    public String showForm(Model model) {
        Contato contato = new Contato();
        model.addAttribute("contato", contato);
        return "cadastroContato";
    }

//    Métodos com lógica

    @PostMapping(path = "/cadastrarContato", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String insert(Contato obj) {
        System.out.println(obj);
        obj = contatoService.save(obj);
        return "index";
    }

    @GetMapping("/listarContatos")
    public String listaContatos(Model model) {
        model.addAttribute("listaContatos", contatoService.findAll());
        return "listarContatos";
    }

    @GetMapping(value = "/nome")
    public String nome(Model model, @RequestParam(name = "nome") String nome) {
        System.out.println(nome+"!!!!");
        List<Contato> contatos =  contatoService.findByNome(nome);
        contatos.forEach(System.out::println);
        model.addAttribute("listaContatos", contatos);
        return "listarContatos";
    }

    @GetMapping(value = "/contato")
    public String contato(Model model, @RequestParam(name = "id") String id) {
        Contato contato = contatoService.findById(Long.decode(id));
        if (contato != null) {
            model.addAttribute("contato", contato);
        }
        return "findContato";
    }

    @GetMapping(value = "/deletaContatoId")
    public String deletaContato(Model model, @RequestParam(name = "id") String id) {
        String retorno = contatoService.deleteById(Long.decode(id));
        model.addAttribute("retorno", retorno);
        return "sucessoErro";
    }

    @GetMapping(value = "/updateContatoId")
    public String updateContato(Model model, @RequestParam(name = "id") String id) {
        Contato contato = contatoService.findById(Long.decode(id));
        model.addAttribute("contato", contato);
        return "updateContato";
    }

    @PostMapping(path = "/saveUpdate", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE})
    public String saveUpdate(Model model,Contato obj, @RequestParam(name = "id") String id) {
        System.out.println(obj);
        obj = contatoService.update(Long.decode(id), obj);
        model.addAttribute("contato", obj);
        return "findContato";
    }


}
