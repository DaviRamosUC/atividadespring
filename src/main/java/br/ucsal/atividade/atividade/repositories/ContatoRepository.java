package br.ucsal.atividade.atividade.repositories;

import br.ucsal.atividade.atividade.entities.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ContatoRepository extends JpaRepository<Contato, Long> {

    List<Contato> findByNome(String nome);

}
