package br.ucsal.atividade.atividade.config;

import br.ucsal.atividade.atividade.entities.Contato;
import br.ucsal.atividade.atividade.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {

    @Autowired
    ContatoRepository contatoRepository;

    @Override
    public void run(String... args) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Contato c1 = new Contato(null, "Davi", "davi.lima@ucsal.edu.br", "Rua a", sdf.parse("26/02/2000"));
        Contato c2 = new Contato(null, "Davi", "davi.brito@ucsal.edu.br", "Rua b", sdf.parse("12/05/1976"));
        Contato c3 = new Contato(null, "Sandra Ancântara", "sandra.alc@ucsal.edu.br", "Rua c", sdf.parse("27/01/1972"));
        contatoRepository.saveAll(Arrays.asList(c1,c2,c3));
    }
}
